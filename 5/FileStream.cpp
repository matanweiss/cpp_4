#include "FileStream.h"
namespace msl
{
	FileStream::FileStream(const char * str)
	{
		fopen_s(&file, str, "w"); // opening the file
	}


	FileStream::~FileStream()
	{
		fclose(file); // closing the file
	}

	FileStream& FileStream::operator<<(const char *str)
	{
		fprintf(file, "%s", str); // printing to the file
		return *this;
	}

	FileStream& FileStream::operator<<(int num)
	{
		fprintf(file, "%d", num); // printing to the file
		return *this;
	}

	void FileStream::endline()
	{
		fprintf(file, "%s", "\n"); // printing \n (going down a row) to the file
	}

}