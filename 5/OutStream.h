#pragma once
#include <cstdio>
namespace msl
{
	class OutStream
	{
	public:
		OutStream();
		~OutStream();

	protected:
		FILE * file;

	public:
		// using << to print
		OutStream & operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
		void endline();
	};

}