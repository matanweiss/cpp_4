#pragma once
#include "OutStream.h"
namespace msl
{
	class FileStream : public OutStream
	{
	public:

		/* function prints \n to the file
		input: none
		output: none */
		void endline();

		//c'tor that gets the file name
		FileStream(const char*  str);
		//d'tor
		~FileStream();
		// the operator << prints to the file. works with integers and strings
		FileStream& operator<<(const char *str);
		FileStream& operator<<(int num);
	};
}

