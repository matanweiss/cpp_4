﻿#include "OutStream.h"
#include "FileStream.h"
#include "Logger.h"
#include "OutStreamEncrypted.h"
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
	// סעיף אחד
	OutStream o = OutStream();
	o<<"I am the Doctor and I'm 1500 years old";
	o.endline();

	// סעיף 2
	FileStream fs = FileStream("Text.txt");
	fs << ("I am the Doctor and I'm 1500 years old");
	fs.endline();

	// סעיף 3
	OutStreamEncrypted ose = OutStreamEncrypted(3);
	ose << ("I am the Doctor and I'm 1500 years old");
	ose.endline();

	//סעיף 4
	Logger l = Logger();
	Logger b = Logger();
	l << "I am the Doctor and I'm 1500 years old";
	l << 4;
	b << 55;
	getchar();
	return 0;
}
