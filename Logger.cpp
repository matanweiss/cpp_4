#include "Logger.h"
#include "OutStream.h"

Logger::Logger()
{
}

Logger::~Logger()
{
}

unsigned int Logger::_logCount = 0;

Logger& operator<<(Logger& l, const char *msg)
{
	if (l._startLine == true) // checking if we are in a start of a line
	{
		l.os << l._logCount / 2; // printing the number of times we printed log
		l.os << " LOG: "; // printing log
	}
	l.setStartLine(); // setting start line to false
	l.os << msg; // printing msg
	l.os.endline(); // going down a row
	l.setStartLine(); // chenging the start line to true
	return l;
}

Logger& operator<<(Logger& l, int num)
{
	if (l._startLine == true)  // checking if we are in a start of a line
	{
		l.os << l._logCount / 2; // printing the number of times we printed log
		l.os << " LOG: "; // printing log
	}
	l.setStartLine(); // setting start line to false
	l.os << num; // printing num
	l.os.endline(); // going down a row
	l.setStartLine(); // chenging the start line to true
	return l;
}

Logger& operator<<(Logger& l, void(*pf)())
{	
	l.os << l._logCount/2;  // printing the number of times we printed log
	l.os << " LOG: "; // printing log
	pf(); // calling the function
	return l;
}

void Logger::setStartLine()
{
	_startLine = !_startLine; // changing start line
	_logCount++; // increasing log count
}