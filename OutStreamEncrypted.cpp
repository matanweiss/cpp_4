#include "OutStreamEncrypted.h"
#include <iostream>
#define MIN 32
#define MAX 126
OutStreamEncrypted::OutStreamEncrypted(unsigned int offset)
{
	this->_offset = offset;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(char *str)
{
	fprintf(stdout, "%s", encrypt(str,_offset)); // printing the str encrypted with the offset
	return *this;
}

char * encrypt(char * str, int offset)
{
	char * res = new char[strlen(str)]; // creating new array in the size of str
	int i = 0, check = 0;
	for (i; i < strlen(str); i++)
	{
		if (int(str[i]) >= MIN && int(str[i]) <= MAX)
		{
			check = str[i] + offset;
			if (check > MAX) // checking if the str+offset has exceeded the max ascii
			{
				check -= MAX - MIN + 1;
			}
			else if (check < MIN)
			{
				check += MAX - MIN + 1;
			}
			res[i] = (char)check;
		}
		else 
		{
			std::cerr << "invalid character!"; // if a char is invalid printing and exiting
			_exit(1);
		}
	}
	res[i] = 0; // putting 0 at end of the char array to make it a string 
	return res; // returning the string
}
