#pragma once
#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{

private:
	unsigned int _offset;

public:
	//c'tor that gets the ffset of the ceaser cipher
	OutStreamEncrypted(unsigned int offset);
	//d'tor
	~OutStreamEncrypted();
	//using << to print the given str
	OutStreamEncrypted& operator<<(char *str);
};
/*
function encrypts a given str using ceaser cipher and the given offset
input: str and offset
output: returnes the encrypted str
*/
char * encrypt(char * str, int offset);
