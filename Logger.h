#pragma once

#include "OutStream.h"
#include <stdio.h>

class Logger
{
	static unsigned int _logCount;
	OutStream os;
	bool _startLine = true;
	void setStartLine();
public:
	// c'tor and d'tor
	Logger();
	~Logger();

	//using the operator << to print. adding log at the start of every string printed
	friend Logger& operator<<(Logger& l, const char *msg);
	friend Logger& operator<<(Logger& l, int num);
	friend Logger& operator<<(Logger& l, void(*pf)());
};
