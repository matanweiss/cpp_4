#pragma once
#include <cstdio>
class OutStream
{
public:
	OutStream();
	~OutStream();

protected:
	FILE * file;

public:
	// using << to print
	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)());
	void endline();
};

