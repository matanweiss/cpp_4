#include "OutStream.h"
#include <stdio.h>



OutStream::OutStream()
{
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(stdout,"%s", str); // printing the str
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(stdout,"%d", num); // printing the num
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)())
{
	pf(); // calling the given function
	return *this;
}


void OutStream::endline()
{
	fprintf(stdout,"%s","\n"); // going down a row
}
